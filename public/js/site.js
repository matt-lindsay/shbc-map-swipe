// site.js
var map;

function init(){
// initiate leaflet map
	map = new L.Map('mapid', { 
	center: [51.235222, -0.575075],
	zoom: 12
})
	

// basemaps
//openstreetmap
var openstreetmap = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors,<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
}).addTo(map);
	
// 2013 Aerial Survey.
var airSurvey = L.tileLayer.wms('https://mapping.surreyheath.gov.uk/WMS9/wms.exe?', {
		attribution: '&copy;Surrey Heath Borough Council ',
		layers: '_2013AirSurvey_WGS84',
		format: 'image/png',
		transparent: false
});
	
	
// basemaps end
	
// historic wms layers
var johnRocque1767 = L.tileLayer.wms(
	'https://mapping.surreyheath.gov.uk/WMS9/wms.exe?', {
	layers: 'JohnRocque_WGS84',
	attribution: '&copy; Surrey Heath Borough Council',
	// opacity: 0.8,
	// bgcolor: '80bde3',
	transparent: true
});

// Windlesham Tithe
var copyrightYear = '???'
var windleshamtithe = L.tileLayer.wms('https://mapping.surreyheath.gov.uk/WMS9/wms.exe?', {
		attribution: '&copy; Crown copyright. All rights reserved. Surrey Heath Borough Council 100018679 ' + copyrightYear,
		layers: 'WindleshamTitheWGS84'
});

// Chobham Camp
var copyrightYear = '???'
var chobhamcamp = L.tileLayer.wms('https://mapping.surreyheath.gov.uk/WMS9/wms.exe?', {
		attribution: '&copy; Crown copyright. All rights reserved. Surrey Heath Borough Council 100018679 ' + copyrightYear,
		layers: 'ChobhamCampWGS84'
});

// scale
	
//	set crs.distance as workaround for proj4leaflet which has no distance() function needed for scalebar
//	crs.distance = L.CRS.Earth.distance;
//	crs.R = 6378137;
	
L.control.scale({imperial: false, maxWidth: 100}).addTo(map);
// scale end
	
// info panel in top right
// this can be updated based on mouse hover over features
// just call the function onMouseover and config the properties the function should use
var info = L.control();

info.onAdd = function (map) {
	this._div = L.DomUtil.create('div', 'info title-box'); // create a div with a class "info"
	this.update();
	return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (properties) {
	this._div.innerHTML = '<h4>Surrey Heath Historic Mapping</h4>';
};

info.addTo(map);
// info panel end
	
	
// layer control
var baseMaps = {
	"OpenStreetMap": openstreetmap,
	"Air Survey 2013": airSurvey,
};
	
var overlayMaps = {
	"John Rocque 1767": johnRocque1767,
	"Windlesham Tithe 1813": windleshamtithe,
	"Chobham Camp": chobhamcamp
};
	
var layersControl = L.control.layers(baseMaps, overlayMaps, {position: 'topright'}).addTo(map);
// layer control end
	
	
// swipe
var overlay = johnRocque1767;
overlay.addTo(map);
var range = document.getElementById('range');

function clip() {
var nw = map.containerPointToLayerPoint([0, 0]),
	se = map.containerPointToLayerPoint(map.getSize()),
	clipX = nw.x + (se.x - nw.x) * range.value;

overlay.getContainer().style.clip = 'rect(' + [nw.y, clipX, se.y, nw.x].join('px,') + 'px)';
}

range['oninput' in range ? 'oninput' : 'onchange'] = clip;
map.on('move', clip);

clip();
// swipe end
	
	
// overlay switch function
map.on('overlayadd', function(e) {
	console.log(e.name + ' is being set as the new swipe overlay.');
	if (e.name == 'John Rocque 1767') {
		map.removeLayer(windleshamtithe);
		map.removeLayer(chobhamcamp);
		overlay = johnRocque1767;
		clip();
		layersControl._update();
	}
	else if (e.name == 'Windlesham Tithe 1813') {
		map.removeLayer(johnRocque1767);
		map.removeLayer(chobhamcamp);
		overlay = windleshamtithe;
		clip();
		layersControl._update();
	}
	else if (e.name == 'Chobham Camp') {
		map.removeLayer(johnRocque1767);
		map.removeLayer(windleshamtithe);
		overlay = chobhamcamp;
		clip();
		layersControl._update();
	}
});
// overlay switch function end
	
} // function init() end
