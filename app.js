var express = require('express');
var app = express();
var port = process.env.PORT || 5000;

app.use(express.static('public'));

app.listen(port, function (err) {
    if (err) {
        throw err;
    }
    console.log('Running server on port ' + port);
});